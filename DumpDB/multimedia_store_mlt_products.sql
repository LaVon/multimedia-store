CREATE DATABASE  IF NOT EXISTS `multimedia_store` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `multimedia_store`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: multimedia_store
-- ------------------------------------------------------
-- Server version	5.6.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `mlt_products`
--

DROP TABLE IF EXISTS `mlt_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mlt_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `genre_id` int(11) DEFAULT NULL,
  `product_title` varchar(50) DEFAULT NULL,
  `cover_img` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`product_id`),
  KEY `genre_id` (`genre_id`),
  CONSTRAINT `mlt_products_ibfk_1` FOREIGN KEY (`genre_id`) REFERENCES `mlt_genres` (`genre_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mlt_products`
--

LOCK TABLES `mlt_products` WRITE;
/*!40000 ALTER TABLE `mlt_products` DISABLE KEYS */;
INSERT INTO `mlt_products` VALUES (1,1,'Linkin Park - Numb','LinkinPark-Numb.jpg'),(2,3,'Armin Van Buuren - A state of trance','AVBAlbumCover2005.jpg'),(3,6,'The Matrix','TheMatrix.jpg'),(4,5,'Evangelion','evangelion.jpg'),(5,7,'How to Train Your Dragon','HowToTrainYourDragon.jpg'),(6,5,'Mononoke-hime','MononokeHime.jpg'),(7,4,'Fracture','Fracture.jpg'),(8,2,'Fever Ray','FeverRay.jpg'),(9,1,'Five Finger Death Punch','FiveFingerDeathPunch.jpg'),(10,1,'Nickelback - All the right reasons','Nickelback-All the right reasons.jpg'),(11,1,'Кукрыниксы - Фаворит солнца','kukryniksy.jpg'),(12,3,'atb - Future memories','ATB-FutureMemories.jpg'),(13,3,'Above & Beyond - Group Therapy','AboveAndBeyond-Group Therapy.jpg'),(14,4,'The Frozen Ground','TheFrozenGround.jpg'),(15,3,'Airwave - Rank1','airwave-rank1.jpg'),(16,4,'Mystic River','MysticRiver.jpg'),(17,1,'Radiohead - Creep','Radiohead-Creep.jpg'),(18,1,'Ten Thousand Firsts','TenThousandFirsts.png'),(19,6,'Minority Report','MinorityReport.jpg'),(20,7,'Open Season','OpenSeason.jpg');
/*!40000 ALTER TABLE `mlt_products` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-29 20:34:19
