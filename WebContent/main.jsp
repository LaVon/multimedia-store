<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Главная</title>
<link type="text/css" rel="stylesheet" href="styles/mlt.style.css" />
<link type="text/css" rel="stylesheet" href="styles/love.music.css" id="changeable" />
<link type="text/css" rel="stylesheet" href="styles/flick/jquery-ui-1.10.4.custom.css" />
<script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.js"></script>
<script type="text/javascript" src="js/jquery.pajinate.js"></script>
<script type="text/javascript" src="js/mlt.script.js"></script>
</head>
<body>
<%@ include file="header.jsp" %>
<div id="wrapper">
 <%@ include file="menu.jsp" %>
  <div id="content">
   <c:forEach items="${records}" var="record">
	   <div>${record.id}</div>
	   <div>${record.title}</div>
	   <div>${record.text}</div>
	   <div>${record.url}</div>
   </c:forEach>
   <h3>Последнии новинки</h3>
   <div id="accordion">
    <c:forEach items="${latestRec}" var="latestRec">
      <h3>${latestRec.productTitle}</h3>
	  <div>
	    <p>
	   	   Жанр: <b>${latestRec.genreTitle}</b><br>
	   	   Тип: <b>${latestRec.typeTitle}</b>
	   	 </p>
      <img src="${pageContext.request.contextPath}${latestRec.imgPath}" alt="${latestRec.productTitle}" height="250" width="200">
      </div>
    </c:forEach>
   </div>
  </div>
</div>
</body>
</html>
