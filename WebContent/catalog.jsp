<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="content">
<!-- form method=post action="ProductServlet" accept-charset="utf-8" -->
<div class="ui-widget">
<select id="type" name ="type">
   <option value="-1">Выберите тип</option>
   <c:forEach items="${type}" var="option">
     <option value="${option.key}" ${param.type == option.key ? 'selected' : ''}>${option.value}</option>
   </c:forEach>
</select>
<select id="genre" name="genre">
   <option value="-1">Выберите жанр</option>
</select>
<input type="text" name="words" id="words"/>
<input id="search" value="Поиск" type="submit">
</div>
<!--/form -->

<!--  form method=post action="CartServlet"-->
<div id="productTable">
<!-- <div class="divRow">
  <div class="divCell">В корзину</div>
  <div class="divCell">Тип</div>
  <div class="divCell">Жанр</div>
  <div class="divCell">Название</div>
 </div> -->
 <div id="itemContainer" class="container">
 <div class="content">
 <c:forEach items="${records}" var="record">
 <div class="productRow rewrite">
   <img src="${pageContext.request.contextPath}${record.imgPath}" alt="${record.productTitle}">
   <div id="checkboxes"><input type="checkbox" name="inCart" id="inCart" value="${record.productId}">В корзину</div>
   <div>${record.typeTitle}</div>
   <div>${record.genreTitle}</div>
   <div>${record.productTitle}</div>
 </div>
 </c:forEach>
 </div>
 <div class="page_navigation"></div>
 </div>
</div> <!-- divTable -->
<input type="submit" id="inCartBtn" name="inCartButton" value="В корзину" />
<!-- /form -->
</div>