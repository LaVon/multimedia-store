  $(document).ready(function() {
	  addAdditionalStyle($("#changeable"));
	  $('#theme a').click(function(event) {
		 event.preventDefault();
		 changeAdditionalStyle($("#changeable"), $(this).attr('href'));
	  });
	  $( "#accordion" ).accordion();
      $('#default').addClass('active');
	  $("li a").click(function(event){
    	   event.preventDefault();
      	   var url = $(this).attr('href');
      	   document.title = $(this).text();
      	   //alert(title);
      	   $("#content").load(url + " #content > *", function() {
      		 setPajinate();
      		 $("#type").combobox({ 
         	    select: function (event, ui) { 
         	        fillGenres('type');
         	        $('#genre').combobox();
         	    } 
         	});
      		$('#search').click(function() { searchByFilter('type', 'genre', 'words'); });
      		$('#inCartBtn').click(function() { inCart(); });
      		$( "#accordion" ).accordion();
      	   });
           $('#menu li').removeClass();
           $(this).parent().addClass('active');   
               
      });
  });
    function addAdditionalStyle(id) {
    	if(sessionStorage.getItem(id))
    	   id.attr('href', sessionStorage.getItem(id));
    }
    function changeAdditionalStyle(id, style) {
		id.attr('href', "styles/" + style);
		sessionStorage.setItem(id, id.attr('href'));
    }
    
    /*---- A jQuery Pagination Plugin -
           http://th3silverlining.com/2010/04/15/pajination-a-jquery-pagination-plugin ---*/
    function setPajinate() 
    {
    	$('#itemContainer').pajinate({
    		items_per_page : 6,
    		nav_label_first : "<<",
    		nav_label_last : ">>",
    		nav_label_prev : "<",
    		nav_label_next : ">",
    		num_page_links_to_display : 7
    		});
    }
    /*--------------------- A jQuery Pagination Plugin   -----------*/
    
    function fillGenres(type) {
    	var callingElement = $('#' + type);
        var genre = $('#genre');
        $.getJSON('products.html?val=' + callingElement.val(), function(opts) {	   
        genre.find('option').remove();//.end()
	        if (opts) {
	           genre.append($('<option/>').val("-1").text("Выберите жанр"));
	           $.each(opts, function(key, value) {
	        	   genre.append($('<option/>').val(key).text(value));
	            });          
	           } 
	        else 
	        {
	        	genre.append($('<option/>').text("Укажите тип"));
	        }
        });
     }
    function inCart() {
    	var selected = [];
    	$('#checkboxes input:checked').each(function() {
    		//alert($(this).attr('value'));
    		selected.push($(this).attr('value'));
    		$(this).attr('checked', false);
    	});
    	$.getJSON("cart.html", { 'val': selected});
    	alert("Товар добавлен в корзину"); 
    }
    function searchByFilter(type, genre, words) {
    	var typeId = $('#'+ type);
    	var genreId = $('#' + genre);
    	var word = $('#' + words);
    	if(typeId.find(":selected").val() < 0 && genreId.find(":selected").val() < 0 &&  !word.val()) {
    		alert("Укажите параметры поиска");
    		return;
    	}
    	$.getJSON('products.html?type=' + typeId.val() + '&genre=' + genreId.val() + '&words=' + word.val(), function(data) {
    		if(data) {
    		var table = document.querySelectorAll('.content');
    		$('.rewrite').remove(); 
    		var pageContext = '/StoreOfMultimedia';
    		$.each(data, function(i, product) {
    			$("<div class='productRow rewrite'/>").appendTo(table)
    			.append($('<img height="200" width="150">')
    			     .attr({'src' : pageContext+product.imgPath, 'alt' : product.productTitle}))
    			.append($("<div id='checkboxes'/>")
    			.append($("<input type='checkbox' id='inCart' name='inCart'>")
    				 .attr("value", product.productId)))
    			.append($("<div/>").text(product.typeTitle))
    			.append($("<div/>").text(product.genreTitle))
    			.append($("<div/>").text(product.productTitle));
    		});
    		setPajinate();
    		}
    		else {
    			alert("Ничего не найдено");
    		}
    	});
    }
    
    /* ---------  jQueryUI combobox - http://jqueryui.com/autocomplete/#combobox  -------*/
    (function($) {
    	$.widget("custom.combobox", {
    	  _create: function() {
    		this.wrapper = $("<span>")
    		  .addClass("custom-combobox")
    		  .insertAfter(this.element);
    		this.element.hide();
    		this._createAutocomplete();
    		this._createShowAllButton();
    	  },
    	  _createAutocomplete: function() {
    		var selected = this.element.children( ":selected" ),
    		 value = selected.val() ? selected.text() : "";
    		this.input=$("<input>")
    		   .appendTo(this.wrapper)
    		   .val(value)
    		   .attr("title", "")
    		   .addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
    		   .autocomplete({
    			   delay: 0,
    			   minLength: 0,
    			   source: $.proxy(this, "_source")
    		   })
    		   .tooltip({
    			   tooltipClass: "ui-state-highlight"
    		   });
    		this._on(this.input, {
    			autocompleteselect: function(event, ui) {
    				ui.item.option.selected = true;
    				this._trigger( "select", event, {
    				 item: ui.item.option
    				});
    			},
    			autocompletechange: "_removeIfInvalid"
    		});
    	 },
    	 _createShowAllButton: function() {
    		 var input = this.input,
    		    wasOpen = false;
    		 $( "<a>" )
    		  .attr( "tabIndex", -1 )
    		  .attr( "title", "Show All Items" )
    		  .tooltip()
    		  .appendTo( this.wrapper )
    		  .button({
    			  icons: {
    				primary: "ui-icon-triangle-1-s"
    			  },
    			  text: false
    		  })
    		  .removeClass( "ui-corner-all" )
    		  .addClass( "custom-combobox-toggle ui-corner-right" )
    		  .mousedown(function() {
    			  wasOpen = input.autocomplete( "widget" ).is( ":visible" );
    		  })
    		  .click(function() {
    			  input.focus();
    			  if ( wasOpen ) {
    				return;
    			  }
    			  input.autocomplete( "search", "" );
    		  });
    	 },
    	 _source: function( request, response ) {
    		 var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
    		 response( this.element.children( "option" ).map(function() {
    			 var text = $( this ).text();
    			 if ( this.value && ( !request.term || matcher.test(text) ) )
    				 return {
	    				 label: text,
	    				 value: text,
	    				 option: this
    			     };
    		 }) );
    	 },
    	 _removeIfInvalid: function( event, ui ) {
    		 if ( ui.item ) {
    		   return;
    		 }
    		 var value = this.input.val(),
    		 valueLowerCase = value.toLowerCase(),
    		 valid = false;
    		 this.element.children( "option" ).each(function() {
    			 if ( $( this ).text().toLowerCase() === valueLowerCase ) {
    				 this.selected = valid = true;
    				 return false;
    			 }
    		 });
    		 if ( valid ) {
    			 return;
    		 }
    		 this.input
    		 .val( "" )
    		 .attr( "title", value + " didn't match any item" )
    		 .tooltip( "open" );
    		 this.element.val( "" );
    		 this._delay(function() {
    			 this.input.tooltip( "close" ).attr( "title", "" );
    			 }, 2500 );
    		 this.input.data( "ui-autocomplete" ).term = "";
    	 },
    	 _destroy: function() {
    		 this.wrapper.remove();
    		 this.element.show();
         }
       });
    })(jQuery);
    /*----------- jQueryUI ----------------*/
    