package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ProductDAO {
	public ArrayList<Product> loadData() {
		  ArrayList<Product> dataList = new ArrayList();
		  try {
		  String sql = "SELECT mlt_genres.type_id, mlt_products.genre_id, product_id, product_title, cover_img, genre_title, type_title " + 
		               "FROM mlt_products, mlt_genres, mlt_types where mlt_products.genre_id = mlt_genres.genre_id and mlt_genres.type_id = mlt_types.type_id";
		  Connection connect = ConnectionDB.getInstanceVar().getConnect();
		  Statement state = connect.createStatement();
		  ResultSet rs = state.executeQuery(sql);
		  dataList = fillProducts(rs);
			 /* while(rs.next()) {
			    Product rec = new  Product();
			    rec.setGenreId(rs.getInt("mlt_products.genre_id"));
			    rec.setTypeId(rs.getInt("mlt_genres.type_id"));
			    rec.setProductId(rs.getInt("product_id"));
				rec.setProductTitle(rs.getString("product_title"));
				rec.setGenreTitle(rs.getString("genre_title"));
				rec.setTypeTitle(rs.getString("type_title"));
				dataList.add(rec);
			  }*/
		  rs.close();
		  state.close();
		  }
		  catch (SQLException ex) {
			  System.out.println("Error in loadData " + ex);  
		  }
		  return dataList;
	}
	
	public Product loadProduct(int id) {
		  Product dataList = new Product();
		  try {
		  String sql = "SELECT mlt_genres.type_id, mlt_products.genre_id, product_id, product_title, genre_title, type_title " + 
	                   "FROM mlt_products, mlt_genres, mlt_types where mlt_products.genre_id = mlt_genres.genre_id and " + 
				       "mlt_genres.type_id = mlt_types.type_id and product_id = ?";
		  Connection connect = ConnectionDB.getInstanceVar().getConnect();
		  PreparedStatement prepState = connect.prepareStatement(sql);
		  prepState.setInt(1, id);
		  ResultSet rs = prepState.executeQuery();	
		  while(rs.next()) {
				dataList.setGenreId(rs.getInt("mlt_products.genre_id"));
				dataList.setTypeId(rs.getInt("mlt_genres.type_id"));
				dataList.setProductId(rs.getInt("product_id"));
				dataList.setProductTitle(rs.getString("product_title"));
				dataList.setGenreTitle(rs.getString("genre_title"));
				dataList.setTypeTitle(rs.getString("type_title"));
		  }
		  rs.close();
		  prepState.close();
		  }
		  catch (SQLException ex) {
			  System.out.println("Error in loadProduct " + ex);  
		  }
		  return dataList;
	}
	public Map<Integer, String> loadType() {
	  Map<Integer, String> res = new HashMap();
	  try {
			String sql = "SELECT type_id, type_title from mlt_types";
			Connection connect = ConnectionDB.getInstanceVar().getConnect();
			Statement state = connect.createStatement();
			ResultSet rs = state.executeQuery(sql);	
			while(rs.next())
				res.put(rs.getInt("type_id"),rs.getString("type_title")); 
			rs.close();
			state.close();
		}
		catch (SQLException ex) {
				  System.out.println("Error in loadType " + ex);  
		}
	  return res;
	}
	public Map<Integer, String> loadGenre(int type_id) {
	  Map<Integer, String> res = new HashMap();
	  try {
		  String sql = "SELECT mlt_genres.genre_id, genre_title from mlt_genres, mlt_types " +
				  	   "where mlt_genres.type_id = mlt_types.type_id and mlt_genres.type_id=?";
		  Connection connect = ConnectionDB.getInstanceVar().getConnect();
		  PreparedStatement prepState = connect.prepareStatement(sql);
		  prepState.setInt(1, type_id);
		  ResultSet rs = prepState.executeQuery();	  
			  while(rs.next()) 
				res.put(rs.getInt("genre_id"), rs.getString("genre_title"));  
		  rs.close();
		  prepState.close();
		}
		catch (SQLException ex) {
				  System.out.println("Error in loadGenre " + ex);  
		}
	  return res;
	}
    
	public ArrayList<Product> searchData(int type_id, int genre_id, String words) {
		ArrayList<Product> res = new ArrayList();
		String sql = "SELECT mlt_genres.type_id, mlt_products.genre_id, product_id, " + 
                     "product_title, genre_title, cover_img, type_title " +
			         "FROM mlt_products, mlt_genres, mlt_types " +
                     "where mlt_products.genre_id = mlt_genres.genre_id and " + 
			         "mlt_genres.type_id = mlt_types.type_id ";
		if(type_id > 0)
			sql += "and mlt_types.type_id = ? ";
		if(genre_id > 0)
			sql += "and mlt_genres.genre_id = ? ";
		if(!words.isEmpty())
			sql += "and product_title like ?";
		try {
			  /*String sql = "SELECT mlt_genres.type_id, mlt_products.genre_id, product_id, " + 
		                   "product_title, genre_title, type_title " +
					       "FROM mlt_products, mlt_genres, mlt_types " +
		                   "where mlt_products.genre_id = mlt_genres.genre_id and " + 
					       "mlt_genres.type_id = mlt_types.type_id " + 
		                   "and mlt_types.type_id = ? and mlt_genres.genre_id = ? " +
					       "and product_title like ?";*/
			  Connection connect = ConnectionDB.getInstanceVar().getConnect();
			  PreparedStatement prepState = connect.prepareStatement(sql);
			  if(type_id > 0)
			     prepState.setInt(1, type_id);
			  if(genre_id > 0)
			    prepState.setInt(2, genre_id);
			  if(!words.isEmpty() && type_id > 0)
			     prepState.setString(3, "%"+words+"%");
			  else if(!words.isEmpty())
				 prepState.setString(1, "%"+words+"%"); 
			  ResultSet rs = prepState.executeQuery();	
			  res = fillProducts(rs);
			  /*while(rs.next()) {
				    Product rec = new  Product();
				    rec.setGenreId(rs.getInt("mlt_products.genre_id"));
				    rec.setTypeId(rs.getInt("mlt_genres.type_id"));
				    rec.setProductId(rs.getInt("product_id"));
					rec.setProductTitle(rs.getString("product_title"));
					rec.setGenreTitle(rs.getString("genre_title"));
					rec.setTypeTitle(rs.getString("type_title"));
					res.add(rec);
			  }*/
			  rs.close();
			  prepState.close();
			}
			catch (SQLException ex) {
					  System.out.println("Error in searchData " + ex);  
			}
		  return res;
	}
	public ArrayList<Product> loadLatest() {
		  ArrayList<Product> dataList = new ArrayList();
		  try {
		     String sql = "select * from ( " +
		    		      "select product_id, product_title, genre_title, type_title, cover_img " + 
		    		      "FROM mlt_products, mlt_genres, mlt_types " +
		    		      "where mlt_genres.genre_id = mlt_products.genre_id and mlt_types.type_id = mlt_genres.type_id " +
		    		      "order by product_id desc "
		    		      + ") as latest group by latest.genre_title order by product_id desc limit 5";
		      Connection connect = ConnectionDB.getInstanceVar().getConnect();
			  Statement state = connect.createStatement();
			  ResultSet rs = state.executeQuery(sql);	  
			  while (rs.next()) {
				  Product rec = new Product();
				  rec.setProductId(rs.getInt("product_id"));
				  rec.setProductTitle(rs.getString("product_title"));
	     		  rec.setGenreTitle(rs.getString("genre_title"));
				  rec.setTypeTitle(rs.getString("type_title"));
				  rec.setImgPath(rs.getString("cover_img"));
				  dataList.add(rec);		  
			  }
		  }
		  catch (SQLException ex) {
			  System.out.println("Error in loadLatest " + ex);
		  }
		  return dataList;
	 }
	
	private ArrayList<Product> fillProducts(ResultSet rs) {
		ArrayList<Product> dataList = new ArrayList();
		try {
		while (rs.next()) {
			  Product rec = new Product();
			  rec.setGenreId(rs.getInt("mlt_products.genre_id"));
			  rec.setTypeId(rs.getInt("mlt_genres.type_id"));
			  rec.setProductId(rs.getInt("product_id"));
		      rec.setProductTitle(rs.getString("product_title"));
			  rec.setGenreTitle(rs.getString("genre_title"));
			  rec.setTypeTitle(rs.getString("type_title"));
			  if(rs.getString("cover_img") != null)
				     rec.setImgPath(rs.getString("cover_img"));
				  else rec.setImgPath("NoImage.png");
			  dataList.add(rec);		  
		  }
		}
		catch (SQLException ex) {
			System.out.println("Error in fillProducts " + ex);
		}
		return dataList;
	}
}
