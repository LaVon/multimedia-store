package model;

public class StaticRecord {
	private int id;
	private String title;
	private String text;
    private String url;
    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }
    public String getTitle() {
    	return title;
    }
    public void setTitle(String title) {
    	this.title = title;
    }
    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }
    public String getUrl() {
    	return url;
    }
    public void setUrl(String url) {
    	this.url = url;
    }
      
}
