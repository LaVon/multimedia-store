package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;

public class StaticRecordsDAO {

  public ArrayList<StaticRecord> loadData() {
	  ArrayList<StaticRecord> dataList = new ArrayList();
	  try {
	  String sql = "Select * from mlt_static_pages";
	  Connection connect = ConnectionDB.getInstanceVar().getConnect();
	  Statement state = connect.createStatement();
	  ResultSet rs = state.executeQuery(sql);	  
		  while(rs.next()) {
		    StaticRecord rec = new StaticRecord();
			rec.setId(rs.getInt("page_id"));
			rec.setTitle(rs.getString("page_title"));
			rec.setText(rs.getString("page_text"));
			//rec.setUrl(rs.getString("url"));
			dataList.add(rec);
		  }
	  rs.close();
	  state.close();
	  }
	  catch (SQLException ex) {
		  System.out.println("Error in loadData " + ex);  
	  }
	  return dataList;
  }
}
