package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.ProductDAO;
import model.Product;

/**
 * Servlet implementation class ProductServlet
 */
@WebServlet("/ProductServlet")
public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ProductDAO recordDAO = new ProductDAO();
    private List<Product> records = null;
    Map<Integer, String> type = null;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductServlet() {
        super();
		records = recordDAO.loadData();
		type = recordDAO.loadType();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    private void writeJSON(HttpServletResponse response, Object data) throws IOException {
    	String json = new Gson().toJson(data);
	    response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(json);
    }
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		request.setAttribute("type", type);
		//if ("XMLHttpRequest".equals(request.getHeader("X-Requested-With"))) {
			if (request.getParameter("val") != null) {
			    int val = Integer.parseInt(request.getParameter("val")); 
			    Map<Integer, String> options = recordDAO.loadGenre(val);
			    writeJSON(response, options);
			}
			else
			if (request.getParameter("type") != null || request.getParameter("genre") != null || 
			    request.getParameter("words") != null) 
			{
				List<Product> rec = recordDAO.searchData(Integer.parseInt(request.getParameter("type")), 
						                                 Integer.parseInt(request.getParameter("genre")), 
						                                 request.getParameter("words"));
					/*---- debugging -----
					for(Product p : rec)
						p.printProducts();
					/* ----- */
				    if(!rec.isEmpty())
					   writeJSON(response, rec); 
				    else
				       writeJSON(response, null); 
			}
		//}
			else {
				request.setAttribute("records", records);
				request.getRequestDispatcher("/catalog.jsp").forward(request, response);
			}    
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("records", records);
		request.getRequestDispatcher("/catalog.jsp").forward(request, response);
	}
}
