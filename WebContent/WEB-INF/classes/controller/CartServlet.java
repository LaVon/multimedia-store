package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Product;
import model.ProductDAO;

/**
 * Servlet implementation class CartServlet
 */
@WebServlet("/CartServlet")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private HttpSession session;
    private List<Product> cartList = new ArrayList();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("doGet cart");
		if (request.getParameterValues("val[]") != null) {
			System.out.println("got val[]");
			session = request.getSession(true);
			if(session.getAttribute("cartList") == null)
				cartList.clear();
			String[] inCart = request.getParameterValues("val[]");
			if(inCart != null) {
				addToCart(inCart, request);
			}
		}
		else {
			/*if(session == null)
			{
				System.out.println("cartList is null");
				request.getRequestDispatcher("/empty.html").forward(request, response);
			}
		else {*/
			System.out.println("in else");
			if(request.getSession().getAttribute("cartList") == null) {
				System.out.println("is null");
				request.getRequestDispatcher("/empty.jsp").forward(request, response);
			}
			else {
			    System.out.println(request.getSession().getAttribute("cartList"));
			request.setAttribute("cartList", request.getSession().getAttribute("cartList"));
			request.getRequestDispatcher("/cart.jsp").forward(request, response);
			}
		  //}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	private void addToCart(String[] inCart, HttpServletRequest request) {	
		System.out.println("in addToCart");
		for (String s : inCart) {
			System.out.println("potential product : " + s);
			ProductDAO recordDAO = new ProductDAO();
			Product product = recordDAO.loadProduct(Integer.parseInt(s));
     		cartList.add(product);
		}
			session.setAttribute("cartList", cartList);
			request.setAttribute("cartList", cartList);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
