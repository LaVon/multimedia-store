package controller;
import java.sql.*;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Product;
import model.ProductDAO;
import model.StaticRecordsDAO;
import model.StaticRecord;

import java.util.*;
/**
 * Servlet implementation class StaticServlet
 */
@WebServlet("/StaticServlet")
public class StaticServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StaticServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		StaticRecordsDAO recordDAO = new StaticRecordsDAO();
		List<StaticRecord> records = recordDAO.loadData();
		ProductDAO latestRecDAO = new ProductDAO();
		List<Product> latestRec = latestRecDAO.loadLatest();
		request.setAttribute("records", records);
		request.setAttribute("latestRec", latestRec);
		request.getRequestDispatcher("/main.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
