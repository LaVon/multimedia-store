<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id="content">
    <p>Товар, отложенный в корзину</p>
	<div id="cartTable">
	<div class="cartRow">
	  <div class="cartCell">Тип</div>
	  <div class="cartCell">Жанр</div>
	  <div class="cartCell">Название</div>
	 </div>
	 <c:forEach items="${cartList}" var="cartList">
	 <div class="cartRow">
	   <div class="cartCell">${cartList.typeTitle}</div>
	   <div class="cartCell">${cartList.genreTitle}</div>
	   <div class="cartCell">${cartList.productTitle}</div>
	 </div>
	 </c:forEach>
	</div>
</div>