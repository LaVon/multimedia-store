package model;

import java.sql.*;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionDB {
	private static ConnectionDB instanceVar = null;
	private Connection connect = null;
	public static ConnectionDB getInstanceVar() {
		System.out.println("in getInstVar");
		if (instanceVar == null)
			instanceVar = new ConnectionDB();
		return instanceVar;
	}
	ConnectionDB() {
		connect = connectionDB();
	}
	private Connection connectionDB() {
		System.out.println("in connectionDB()");
		  Connection conn = null;
		  try {
			  System.out.println("in try");
			  InitialContext context = new InitialContext();
			  DataSource dataSource = (DataSource) context.lookup("java:comp/env/jdbc/multimedia_store");
			  conn = dataSource.getConnection();
			  System.out.println("conn " + conn);
			  /*Class.forName("com.mysql.jdbc.Driver");
			  conn = DriverManager.getConnection("jdbc:mysql://localhost/multimedia_store", "root", "1111");
			  System.out.println("got connection");*/
		  }
		  /*catch (ClassNotFoundException e1) {
			  // JDBC driver class not found, print error message to the console
			  System.out.println(e1.toString());
		  }*/
		  catch (SQLException e2) {
			  // Exception when executing java.sql related commands, print error message to the console
			  System.out.println(e2.toString());
		  }
		  catch (NamingException  e3) {
			  // other unexpected exception, print error message to the console
			  System.out.println(e3.toString());
		  }
		  return conn;
	  }  
	public Connection getConnect() {
		return connect;
	}
}
