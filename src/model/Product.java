package model;

public class Product {
    private int productId;
    private int genreId;
    private int typeId;
    private String productTitle;
    private String genreTitle;
    private String typeTitle;
    private String imgPath;
    
    /*--- debugging ---*/
    public void printProducts() {
    	System.out.println("product id: " + productId);
    	System.out.println("genre id: " + genreId);
    	System.out.println("type id: " + typeId);
    	System.out.println("product title: " + productTitle);
    	System.out.println("genre title: " + genreTitle);
    	System.out.println("type title: " + typeTitle);
    }
    /* ------- */
    
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public int getGenreId() {
		return genreId;
	}
	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}
	public void setImgPath(String img) {
		imgPath = "/img/covers_img/" + img;
	}
	public String getImgPath() {
		return imgPath;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public String getProductTitle() {
		return productTitle;
	}
	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
	public String getGenreTitle() {
		return genreTitle;
	}
	public void setGenreTitle(String genreTitle) {
		this.genreTitle = genreTitle;
	}
	public String getTypeTitle() {
		return typeTitle;
	}
	public void setTypeTitle(String typeTitle) {
		this.typeTitle = typeTitle;
	}
	
}
